""" Projet 2 : Implémantation en python3 d'un pendu """

__author__ = "Noemie Muller"
__matricule__ = "000458865"
__date__ = "24/10/2017"
__cours__ = "info"
__titulaire__ = "Thierry Massart"
__groupe_tp_ = "5"
__asistant__ = "Fabio Sciamannini"

from random import randint
import turtle

def mot_aleatoire() :
    """genere aleatoirement un mot a deviner issu de la liste du fichier mots.txt
    """
    fichier = open('mots.txt')
    liste_de_mots = []
    for line in fichier : #cree une liste composee de chaque mot du fichier
        liste_de_mots.append(line.strip())
    i = randint(0, (len(liste_de_mots)-1)) #genere aleatoirement un indice correspondant a un mot de la liste
    return liste_de_mots[i]

def traits() :
    """trace un trait par lettre composant le mot a deviner
    """
    i = 0
    turtle.up()
    turtle.goto(-250,-100)
    while i < nombre_de_lettres :
        turtle.down()
        turtle.forward(30)
        turtle.up()
        turtle.forward(10)
        i += 1

def dessin() :
    """dessine le pendu au fur et a mesure des erreurs du joueur
    """
    if erreurs == 1 : #dessin de la potence
        turtle.up()
        turtle.setposition(-300,0)
        turtle.down()
        turtle.forward(500)
        turtle.up()
        turtle.goto(-100,0)
        turtle.left(90)
        turtle.down()
        turtle.forward(300)
        turtle.right(90)
        turtle.forward(150)
        turtle.right(90)
        turtle.forward(50)
    elif erreurs == 2 : #dessin de la tete
        turtle.up()
        turtle.setposition(50,250)
        turtle.setheading(180)
        turtle.down()
        turtle.circle(20)
    elif erreurs == 3 : #dessin du corps
        turtle.up()
        turtle.setposition(50,210)
        turtle.down()
        turtle.setheading(270)
        turtle.forward(80)
    elif erreurs == 4 : #dessin du bras gauche
        turtle.up()
        turtle.setposition(50,200)
        turtle.down()
        turtle.setheading(-55)
        turtle.forward(50)
    elif erreurs == 5 : #dessin du bras droit
        turtle.up()
        turtle.setposition(50,200)
        turtle.down()
        turtle.setheading(235)
        turtle.forward(50)
    elif erreurs == 6 : #dessin de la jambe gauche
        turtle.up()
        turtle.setposition(50,130)
        turtle.down()
        turtle.setheading(-55)
        turtle.forward(60)
    elif erreurs == 7 : #dessin de la jambe droite
        turtle.up()
        turtle.setposition(50,130)
        turtle.down()
        turtle.setheading(235)
        turtle.forward(60)

def mauvaises_lettres() :
    """affiche les mauvaises lettres donnees sous le dessin
    """
    turtle.up()
    turtle.goto(-240 + 30*erreurs,-150)
    turtle.write(lettre, font = ("Arial", 18, "normal"))

def bonnes_lettres() :
    """affiche les bonnes lettres donnees sur les tirets
    """
    turtle.up()
    turtle.goto(-240+40*indice,-95)
    turtle.write(lettre, font = ("Arial", 18, "normal"))

if __name__ == '__main__':

    mot = mot_aleatoire()
    nombre_de_lettres = len(mot)
    traits()
    erreurs = 0
    lettres_trouvees = 0
    liste_mauvaises_lettres = []

    while erreurs < 7 and lettres_trouvees < len(mot) :  #boucle de jeu
        lettre = turtle.textinput('input','Lettre:').lower()
        while len(lettre) != 1 or lettre.isdigit() or not lettre.isalpha() or lettre in liste_mauvaises_lettres : 
            #boucle de verification d'encodage
            lettre = turtle.textinput('input','Lettre:').lower()
        if lettre not in mot : #si la lettre n'est pas dans le mot
            erreurs += 1
            dessin()
            mauvaises_lettres()
            liste_mauvaises_lettres.append(lettre)
        else : #si la lettre est dans le mot
            for i in range(len(mot)) :
                if lettre == mot[i] :
                    indice = i
                    bonnes_lettres()
                    lettres_trouvees += 1
    if erreurs == 7 :
        turtle.bgcolor("red")